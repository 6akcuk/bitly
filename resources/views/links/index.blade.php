@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Links</h1>

        <div class="card">
            <div class="card-body">
                <div class="text-right" style="margin-bottom: 20px;">
                    <a href="{{ route('links.create') }}" class="btn btn-primary">
                        Add Link
                    </a>
                </div>

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>URL</th>
                        <th>Link</th>
                        <th>Views</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($links as $link)
                        <tr>
                            <td>{{ $link->id }}</td>
                            <td>{{ $link->name }}</td>
                            <td>{{ $link->url }}</td>
                            <td>
                                <a href="{{ $link->link }}" target="_blank">
                                    {{ $link->link }}
                                </a>
                            </td>
                            <td>{{ $link->views }}</td>
                            <td>
                                <form action="{{ route('links.destroy', $link) }}" method="post">
                                    @method('DELETE')
                                    @csrf

                                    <button class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" align="center">
                                No links added.
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
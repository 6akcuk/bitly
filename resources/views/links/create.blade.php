@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('links.store') }}" method="post">
                    @csrf

                    @include('links.form', ['link' => new App\Link])

                    <button type="submit" class="btn btn-primary">
                        Create
                    </button>

                    <a href="{{ route('links.index') }}" class="btn btn-outline-secondary">
                        Cancel
                    </a>
                </form>
            </div>
        </div>
    </div>
@endsection
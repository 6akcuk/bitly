<div class="form-group">
    <label for="name">Name</label>
    <input type="text" id="name" name="name" class="form-control" value="{{ old('name', $link->name) }}" required>
    @if ($errors->has('name'))
        <small class="form-text text-muted">
            {{ $errors->first('name') }}
        </small>
    @endif
</div>

<div class="form-group">
    <label for="url">URL</label>
    <input type="text" id="url" name="url" class="form-control" value="{{ old('url', $link->url) }}" required>
    @if ($errors->has('url'))
        <small class="form-text text-muted">
            {{ $errors->first('url') }}
        </small>
    @endif
</div>


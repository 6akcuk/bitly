<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = [
        'id',
        'name',
        'url'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function getLinkAttribute()
    {
        return route('links.go', $this->code);
    }


    // Scopes
    public function scopeCode($query, $code)
    {
        return $query->where('code', $code);
    }
}

<?php

namespace App\Http\Controllers;

use App\Contracts\LinkFactoryContract;
use App\Contracts\LinksRepositoryContract;
use App\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LinksController extends Controller
{
    /**
     * @var LinksRepositoryContract
     */
    public $links;

    /**
     * @var LinkFactoryContract
     */
    public $factory;

    public function __construct(LinksRepositoryContract $links, LinkFactoryContract $factory)
    {
        $this->links = $links;
        $this->factory = $factory;
    }

    /**
     * View all user's links.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $links = $this->links->getAllByUser(auth()->user());

        return view('links.index', compact('links'));
    }

    /**
     * Create form for new link.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('links.create');
    }

    /**
     * Create a new link.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'url' => 'required|url',
            'name' => 'required'
        ]);

        $this->factory->make(auth()->user(), $request->all());

        session()->flash('message', 'Link added.');

        return redirect()->route('links.index');
    }

    /**
     * Delete a link.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $link = $this->links->find($id);
        $link->delete();

        session()->flash('message', 'Link deleted.');

        return redirect()->route('links.index');
    }

    /**
     * Move to the url.
     *
     * @param $code
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function go($code)
    {
        $link = $this->links->getByCode($code);
        DB::table('links')->where('id', $link->id)->increment('views');

        return redirect($link->url);
    }
}

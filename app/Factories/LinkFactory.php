<?php

namespace App\Factories;

use App\Contracts\LinkFactoryContract;
use App\Link;
use App\User;

class LinkFactory implements LinkFactoryContract
{
    public function make(User $user, array $attributes): Link
    {
        $link = Link::where('user_id', $user->id)
            ->where('url', $attributes['url'])
            ->first();

        if ( ! $link) {
            $link = new Link;
            $link->fill($attributes);
            $link->user()->associate($user);
            $link->code = $this->generateCode();
            $link->save();
        }

        return $link;
    }

    public function generateCode(): string
    {
        $code = str_random(6);

        while (Link::where('code', $code)->exists()) {
            $code = str_random(6);
        }

        return $code;
    }
}
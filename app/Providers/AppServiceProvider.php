<?php

namespace App\Providers;

use App\Contracts\LinkFactoryContract;
use App\Contracts\LinksRepositoryContract;
use App\Factories\LinkFactory;
use App\Repositories\LinksRepository;
use Doctrine\DBAL\Types\Type;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Type::addType('char', 'Doctrine\DBAL\Types\StringType');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Repositories
        $this->app->bind(LinksRepositoryContract::class, LinksRepository::class);

        // Factories
        $this->app->bind(LinkFactoryContract::class, LinkFactory::class);
    }
}

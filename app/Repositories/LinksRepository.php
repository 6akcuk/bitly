<?php

namespace App\Repositories;

use App\Contracts\LinksRepositoryContract;
use App\Link;
use App\User;
use Illuminate\Database\Eloquent\Collection;

class LinksRepository implements LinksRepositoryContract
{
    /**
     * Get all user's links.
     *
     * @param User $user
     * @return Collection
     */
    public function getAllByUser(User $user): Collection
    {
        return $user->links()->get();
    }

    /**
     * Get link instance by unique code.
     *
     * @param string $code
     * @return Link
     */
    public function getByCode(string $code): Link
    {
        return Link::code($code)->first();
    }

    /**
     * Get link by id.
     *
     * @param int $id
     * @return Link
     */
    public function find(int $id): Link
    {
        return Link::find($id);
    }
}
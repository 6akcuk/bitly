<?php

namespace App\Contracts;

use App\Link;
use App\User;

interface LinkFactoryContract
{
    /**
     * Make a new link instance.
     *
     * @param array $attributes
     * @return Link
     */
    public function make(User $User, array $attributes): Link;

    /**
     * Generate unique link code.
     *
     * @return string
     */
    public function generateCode(): string;
}
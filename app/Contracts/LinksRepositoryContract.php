<?php

namespace App\Contracts;

use App\Link;
use App\User;
use Illuminate\Database\Eloquent\Collection;

interface LinksRepositoryContract
{
    /**
     * Get all user's links.
     *
     * @return Collection
     */
    public function getAllByUser(User $user): Collection;

    /**
     * Get link instance by unique code.
     *
     * @param string $code
     * @return Link
     */
    public function getByCode(string $code): Link;

    /**
     * Get link by id.
     *
     * @param int $id
     * @return Link
     */
    public function find(int $id): Link;
}